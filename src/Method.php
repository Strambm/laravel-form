<?php

namespace Cetria\Laravel\Form;

enum Method
{
    case POST;
    case GET;
    case DELETE;
    case PUT;
    case PATCH;
}
