<?php

namespace Cetria\Laravel\Form;

use Error;
use ErrorException;
use JsonSerializable;
use Cetria\Laravel\Form\Traits\HasInputContent;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Hidden;

class FormAttributes
implements JsonSerializable
{
    use HasInputContent {
        addContent as TraitAddContent;
    }

    /** @var string */
    protected $action;
    /** @var string */
    protected $method;

    /**
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\ConstructTest
     */
    public function __construct(string $action = '', Method $method = Method::POST)
    {
        $this->setAction($action);
        $this->setMethod($method);
    }

    /**
     * @throws Error
     * @throws ErrorException
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\AddContentTest
     */
    public function addContent(Input $entity, string $key = null): static
    {
        if(!$this->isReservedAttribute($entity)) {
            throw new Error('Unsupported attribute with name: \'' . $entity->getName() . '\'.');
        }
        return $this->TraitAddContent($entity, $key);
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\GetActionTest
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\GetMethodTest
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string[]
     */
    public function getReservedAttributeNames(): array
    {
        return [
            '_method',
        ];
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\IsReservedAttributeTest
     */
    public function isReservedAttribute(Input $attribute): bool
    {
        $name = $attribute->getName();
        return $this->isReservedAttributeName($name);
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\IsReservedAttributeNameTest
     */
    public function isReservedAttributeName(string $name): bool
    {
        return in_array($name, static::getReservedAttributeNames());
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\JsonSerializeTest
     */
    public function jsonSerialize(): array
    {
        $attributes = get_object_vars($this);
        return $attributes;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\SetActionTest
     */
    public function setAction(string $action): static
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\FormAttributes\SetMethodTest
     */
    public function setMethod(Method $method): static
    {
        if(in_array($method->name, ['GET', 'POST'])) {
            $this->method = $method->name;
            $this->setContent([]);
        } else {
            $this->method = Method::POST->name;
            $this->addContent(new Hidden('_method', $method->name));
        }
        return $this;
    }
}
