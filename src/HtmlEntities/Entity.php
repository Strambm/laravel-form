<?php

namespace Cetria\Laravel\Form\HtmlEntities;

use JsonSerializable;
use Illuminate\View\View;
use Cetria\Helpers\ArrayHelper\ArrayHelper;
use Cetria\Laravel\Form\Exception\ViewDoesNotExistException;
use Illuminate\Support\Facades\View as FacadeView;

abstract class Entity
implements JsonSerializable
{
    /** @var int $id */
    protected $id;
    /** @var int $idCounter */
    protected static $idCounter = 0;
    /** @var string $view */
    protected $view;

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Entity\ConstructorTest
     */
    public function __construct()
    {
        $this->id = ++ self::$idCounter;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Entity\ConstructorTest
     */
    public function getId(): string
    {
        return 'cetria-form-' . $this->id;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Entity\GetViewNameTest
     */
    public function getViewName(): string
    {
        return $this->view;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Entity\jsonSerializeTest
     */
    public function jsonSerialize(): array
    {
        $attributes = get_object_vars($this);
        $attributes = ArrayHelper::unsetKeyIfExist($attributes, 'view');
        return $attributes;
    }

    /**
     * @throws ViewDoesNotExistException
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Entity\SetViewNameTest
     */
    public function setViewName(string $viewName): static
    {
        if(FacadeView::exists($viewName)) {
            $this->view = $viewName;
        } else {
            throw new ViewDoesNotExistException($viewName);
        }
        return $this;
    }

    public function render(): View
    {
        return FacadeView::make($this->getViewName(), ['entity' => $this]);
    }

    public abstract function changeNamePrefix(string $from, string $to): static;
}
