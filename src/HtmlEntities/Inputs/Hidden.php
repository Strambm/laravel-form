<?php

namespace Cetria\Laravel\Form\HtmlEntities\Inputs;

class Hidden extends Input
{  
    protected $view = 'cetria-form::inputs.hidden';

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Hidden\GetInputTypeTest
     */
    public function getInputTypeAttribute(): string
    {
        return 'hidden';
    }
}
