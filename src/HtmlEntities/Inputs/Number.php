<?php

namespace Cetria\Laravel\Form\HtmlEntities\Inputs;

use Exception;
use TypeError;
use Cetria\Helpers\NumberHelper\NumberHelper;
use Cetria\Laravel\Form\Exception\BadNumberBetweenException;

/**
 * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\ConstructorTest
 */
class Number extends Input
{  
    /** @var ?float $step */
    protected $step = null;
    /** @var float $min */
    protected $min = null;
    /** @var float $max */
    protected $max = null;

    /**
     * @var \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\GetInputTypeTest
     */
    public function getInputTypeAttribute(): string
    {
        return 'number';
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\GetMaxTest
     */
    public function getMax(): float|null
    {
        return $this->max;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\GetMinTest
     */
    public function getMin(): float|null
    {
        return $this->min;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\GetStepTest
     */
    public function getStep(): string
    {
        if(is_null($this->step)) {
            $value = 1;
        } else {
            $value = $this->step;
        }
        return number_format($value, NumberHelper::decimals($value), '.', '');
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\GetValueTest
     */
    public function getValue(): string
    {
        if(is_null($this->value)) {
            return '';
        } else {
            if(is_null($this->step)) {
                $decimals = NumberHelper::decimals($this->value);
            } else {
                $decimals = NumberHelper::decimals($this->step);
            }
            return number_format(NumberHelper::floor($this->value, $decimals), $decimals, '.', '');
        }
    }

    /**
     * @throws BadNumberBetweenException
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\SetMaxTest
     */
    public function setMax(float|null $max, bool $rewriteAbsolutly = true): static
    {
        if(
            $rewriteAbsolutly
            || is_null($this->max)
            || (
                !is_null($this->max) 
                    && !is_null($max)
                    && $max < $this->max
            )
        ) {
            $this->max = $max;
        }
        $this->validateBetween();
        return $this;
    }

    /**
     * @throws BadNumberBetweenException
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\SetMinTest
     */
    public function setMin(float|null $min, bool $rewriteAbsolutly = true): static
    {
        if(
            $rewriteAbsolutly
            || is_null($this->min)
            || (
                !is_null($this->min) 
                    && !is_null($min)
                    && $min > $this->min
            )
        ) {
            $this->min = $min;
        }
        $this->validateBetween();
        return $this;
    }

    /**
     * @throws BadNumberBetweenException
     */
    private function validateBetween(): void
    {
        if(
            !is_null($this->min)
                && !is_null($this->max)
                && $this->min > $this->max
        ) {
            throw new BadNumberBetweenException($this->min, $this->max);
        }
    }   

    /**
     * @throws Exception
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\SetStepTest
     */
    public function setStep(float $step): static
    {
        if($step <= 0) {
            throw new Exception('step must have value greater than 0');
        } 
        $this->step = $step;
        return $this;
    }

    /**
     * @throws TypeError
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number\SetValueTest
     */
    public function setValue(string|int|float|null $value): static
    {
        if(is_null($value) || $value == '') {
            $this->value = null;
        } elseif(is_numeric($value)) {
            $this->value = (float) $value;
        } else {
            throw new TypeError('Value \'' . $value . '\' cannot be converted to float');
        }
        return $this;
    } 
}
