<?php

namespace Cetria\Laravel\Form\HtmlEntities\Inputs;

class Enumerable extends Input
{  
    /** @var EnumerableOption[] $options */
    protected $options = [];
    protected $view = 'cetria-form::inputs.select';

    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    public function getInputTypeAttribute(): string
    {
        return '';
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Enumerable\AddOptionTest
     */
    public function addOption(EnumerableOption $option): static
    {
        $this->options[$option->getValue()] = $option;
        return $this;
    }

    /**
     * @param EnumerableOption[] $options
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Enumerable\AddOptionsTest
     */
    public function addOptions(array $options): static
    {
        $this->options = [];
        foreach($options as $option) {
            $this->addOption($option);
        }
        return $this;
    }

    /**
     * @return EnumerableOption[]
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Enumerable\GetOptionsTest
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
