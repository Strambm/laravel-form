<?php

namespace Cetria\Laravel\Form\HtmlEntities\Inputs;

use JsonSerializable;

class EnumerableOption
implements JsonSerializable
{  
    /** @var string $value */
    protected $value;
    /** @var null|string $description */
    protected $description;

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption\ConstructorTest
     */
    public function __construct(string $value, string|null $description = null)
    {
        $this->setValue($value);
        $this->setDescription($description);
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption\GetDescriptionTest
     */
    public function getDescription(): string
    {
        if(is_null($this->description)) {
            return $this->getValue();
        }
        return $this->description;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption\GetValueTest
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption\SetDescriptionTest
     */
    public function setDescription(string|null $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption\SetValueTest
     */
    public function setValue(string $value): static
    {
        $this->value = $value;
        return $this;
    }

    public function jsonSerialize(): array
    {
        $attributes = get_object_vars($this);
        return $attributes;
    }
}
