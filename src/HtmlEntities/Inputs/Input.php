<?php

namespace Cetria\Laravel\Form\HtmlEntities\Inputs;

use Cetria\Laravel\Form\HtmlEntities\Entity;
use JsonSerializable;

class Input extends Entity 
implements JsonSerializable
{
    /** @var string $name */
    protected $name = null;
    /** @var string $value */
    protected $value;

    /** @var bool $isRequired */
    protected $isRequired = false;
    /** @var ?string $label */
    protected $label = null;
    protected $view = 'cetria-form::formGroup';

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\ConstructorTest
     */
    public function __construct(string $name, string|null $value = null)
    {
        parent::__construct();
        $this->setName($name);
        $this->setValue($value);
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\ChangeNamePrefixTest
     */
    public function changeNamePrefix(string $from, string $to): static
    {
        return $this->setName(str_replace($from, $to, $this->name));
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetInputTypeTest
     */
    public function getInputTypeAttribute(): string
    {
        return 'text';
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetLabelTest
     */
    public function getLabel(): string
    {
        return (string) $this->label;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetNameTest
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetRequiredTest
     */
    public function getRequired(): bool
    {
        return $this->isRequired;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetValueTest
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\HasLabelTest
     */
    public function hasLabel(): bool
    {
        return !is_null($this->label);
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\jsonSerializeTest
     */
    public function jsonSerialize(): array
    {
        $attributes = parent::jsonSerialize();
        $attributes['type'] = $this->getInputTypeAttribute();
        return $attributes;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\MakeFromAttributeTest
     */
    public static function makeFromAttribute(Input $attribute): static|Input
    {
        if(get_class($attribute) != static::class) {
            $newAttribute = (new static($attribute->name, $attribute->value))
                ->setRequired($attribute->isRequired);
            return $newAttribute;
        } else {
            return $attribute;
        }
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\SetLabelTest
     */
    public function setLabel(string|null $label): static
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\SetNameTest
     */
    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\SetValueTest
     */
    public function setValue(string|null $value): static
    {
        $this->value = (string) $value;
        return $this;
    }  

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\SetRequiredTest
     */
    public function setRequired(bool $isRequired = true): static
    {
        $this->isRequired = $isRequired;
        return $this;
    } 
}
