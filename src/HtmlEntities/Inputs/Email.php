<?php

namespace Cetria\Laravel\Form\HtmlEntities\Inputs;

class Email extends Input
{  
    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Email\GetInputTypeTest
     */
    public function getInputTypeAttribute(): string
    {
        return 'email';
    }
}
