<?php

namespace Cetria\Laravel\Form\HtmlEntities;

use Cetria\Laravel\Form\Traits\HasInputContent;

class InputBlock extends Entity
{
    use HasInputContent;

    
    protected $view = 'cetria-form::inputBlock';

    public function jsonSerialize(): array
    {
        $attributes = parent::jsonSerialize();
        $attributes['content'] = $this->getContent();
        return $attributes;
    }
}
