<?php

namespace Cetria\Laravel\Form\Traits;

use Cetria\Laravel\Form\Form;
use Cetria\Laravel\Form\FormAttributes;
use Cetria\Laravel\Form\HtmlEntities\DynamicBlock;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Cetria\Laravel\Form\Exception\UnsupportedMethodException;

trait UseCastToForm
{
    protected $toFormRelatedModels = [];

    /**
     * @see \Cetria\Laravel\Form\Tests\Traits\UseCastToForm\ExtensionFormForRelatedModelsTest
     * @throws UnsupportedMethodException
     */
    protected function extensionFormForRelatedModels(Form $form): Form
    {
        foreach($this->getToFormRelatedModels() as $relationName => $modelClass) {
            $relation = $this->$relationName();
            $relatedInputBlock = $this->getInputBlockInstanceForRelation($relation);
            $relatedInputBlock->setNamePrefix($relationName . '.');
            $relatedInputBlock->setContent((new $modelClass())->toForm(false)->getContent());
            $form->addContent($relatedInputBlock);
        }
        return $form;
    }

    private function getInputBlockInstanceForRelation(Relation $relation): InputBlock
    {
        switch(true)
        {
            case $relation instanceof HasMany:
                return new DynamicBlock();
            default:
                return new InputBlock();
        }
    }

    

    /**
     * @see \Cetria\Laravel\Form\Tests\Traits\UseCastToForm\GetToFormRelatedModelsTest
     * @throws UnsupportedMethodException
     */
    public function getToFormRelatedModels(): array
    {
        $result = [];
        foreach($this->toFormRelatedModels as $relationName => $modelClass) {
            if(is_integer($relationName)) {
                $relationName = $modelClass;
                $this->validateRelation($relationName);
                $relation = $this->$relationName();
                $modelClass = $relation->getRelated()::class;
            } else {
                $this->validateRelation($relationName);
                $relation = $this->$relationName();
            }
            $this->validateRelatedModel($modelClass);

            $result[$relationName] = $modelClass;
        }
        return $result;
    }

    private function validateRelatedModel(string $modelClass): void
    {
        if(!is_subclass_of($modelClass, CastToFormInterface::class)) {
            throw new UnsupportedMethodException('Class \'' . $modelClass . '\' must be instanceof \'' . CastToFormInterface::class . '\'');
        }
    }

    private function validateRelation(string $relationName): void
    {
        if(!method_exists($this, $relationName)) {
            throw new UnsupportedMethodException('Class \'' . static::class . '\' doesnt have method with name \'' . $relationName . '\'.');
        }
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\Traits\UseCastToForm\ToFormTest
     * @throws UnsupportedMethodException
     */
    public function toForm(bool $isUpdate = true): Form
    {
        $attributes = $this->makeFormAttributesFromModel();
        $form = new Form(new FormAttributes(), $attributes);
        if(method_exists($this, 'getValidator'))
        {
            $form->applyValidators($this->getValidator());
        }
        if(!$isUpdate) {
            $form = $this->extensionFormForRelatedModels($form);
        }
        return $form;
    }

    private function makeFormAttributesFromModel(): array
    {
        $attributes = [];
        foreach($this->getFillable() as $key) {
            $attributes[$key] = new Input($key, $this->getAttributeFromArray($key));
        }
        foreach($this->getCasts() as $key => $type) {
            if(!array_key_exists($key, $attributes)) {
                continue;
            }
            $attributeClass = $this->getAttributeClassForType($type);
            $attributes[$key] = $attributeClass::makeFromAttribute($attributes[$key]);
        }
        return $attributes;
    }

    private static function getAttributeClassForType(string $type): string
    {
        switch($type) {
            case 'integer':
            case 'int':
            case 'float':
            case 'double':
                return Number::class;
            default:
                return Input::class;
        }
    }

    /**
     * @param string $key
     * @return mixed
     * @see \Illuminate\Database\Eloquent\Concerns\HasAttributes
     */
    protected abstract function getAttributeFromArray($key);

    /**
     * @return array attributeName => castType
     * @see \Illuminate\Database\Eloquent\Concerns\HasAttributes
     */
    public abstract function getCasts();

    /**
     * @return array
     * @see \Illuminate\Database\Eloquent\Concerns\GuardsAttributes
     */
    public abstract function getFillable();
}
