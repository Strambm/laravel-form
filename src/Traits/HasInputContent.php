<?php

namespace Cetria\Laravel\Form\Traits;

use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use ErrorException;
use Cetria\Laravel\Form\HtmlEntities\Entity;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\Exception\AttributeDoesNotExistException;

trait HasInputContent
{
    /** @var Entity[] $content */
    protected $content = [];
    /** @var string $namePrefix */
    protected $namePrefix = '';

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock\AddContentTest
     * @throws ErrorException
     */
    public function addContent(Entity $entity): static
    {
        if($entity instanceof Input) {
            $key = $entity->getName();
        } elseif($entity instanceof InputBlock) {
            $key = $entity->getNamePrefix();
        }

        $key = $this->namePrefix . $key;
        
        if($entity instanceof Input) {
            $entity->setName($key);
        } elseif($entity instanceof InputBlock) {
            $entity->setNamePrefix($key);
        }

        $this->content[$key] = $entity;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\Traits\HasInputContent\ChangeNamePrefixTest
     */
    public function changeNamePrefix(string $from, string $to): static
    {
        if($from == '') {
            return $this->setNamePrefix($to . $this->getNamePrefix());
        } else {
            return $this->setNamePrefix(str_replace($from, $to, $this->namePrefix));
        }   
    }

    /**
     * @return Entity[]
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock\GetContentTest
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock\GetContentEntityTest
     */
    public function getContentEntity(string $key): Entity
    {
        if($this->hasEntityExists($key)) {
            return $this->getContent()[$key];
        }
        throw new AttributeDoesNotExistException($key);
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\Traits\HasInputContent\GetNamePrefixTest
     */
    public function getNamePrefix(): string
    {
        return $this->namePrefix;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock\HasEntityExistsTest
     */
    public function hasEntityExists(string $key): bool
    {
        foreach($this->getContent() as $contentKey => $content) {
            if(
                $content instanceof InputBlock
                    && $content->hasEntityExists($key)
                || $content instanceof InputBlock
                    && $contentKey == $key
                || $content instanceof Input
                    && $content->getName() == $key
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Entity[]
     * @see \Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock\SetContentTest
     */
    public function setContent(array $content): static
    {
        $this->content = [];
        foreach($content as $key => $input) {
            $this->addContent($input, $key);
        }
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\Traits\HasInputContent\SetNamePrefixTest
     */
    public function setNamePrefix(string $prefix): static
    {
        if($prefix == $this->namePrefix) {
            return $this;
        }

        $oldPrefixName = $this->namePrefix;
        $this->namePrefix = $prefix;
        $oldContent = $this->content;
        $this->setContent([]);
        foreach($oldContent as $contentEntity) {
            $contentEntity->changeNamePrefix($oldPrefixName, '');
            $this->addContent($contentEntity);
        }

        return $this;
    }
}
