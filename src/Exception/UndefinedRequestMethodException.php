<?php

namespace Cetria\Laravel\Form\Exception;

use Cetria\Laravel\Form\Method;
use Exception;

class UndefinedRequestMethodException extends Exception
{
    public function __construct(string $requestMethod, int $code = 0, $previous = null )
    {
        parent::__construct($this->msg($requestMethod), $code, $previous);
    }

    private function msg(string $requestMethod): string
    {
        return 'Undefined method: \'' . $requestMethod . '\', defined methods list is in enum: \'' . Method::class. '\'';
    }
}
