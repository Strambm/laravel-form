<?php

namespace Cetria\Laravel\Form\Exception;

use Exception;

class AttributeDoesNotExistException extends Exception
{
    public function __construct(string $key, int $code = 0, $previous = null )
    {
        parent::__construct($this->msg($key), $code, $previous);
    }

    private function msg(string $attributeName): string
    {
        return 'Attribute : \'' . $attributeName . '\' does not exist!';
    }
}
