<?php

namespace Cetria\Laravel\Form\Exception;

use InvalidArgumentException;

class BadNumberBetweenException extends InvalidArgumentException
{
    public function __construct(float $from, float $to, int $code = 0, $previous = null )
    {
        parent::__construct($this->msg($from, $to), $code, $previous);
    }

    private function msg(float $from, float $to): string
    {
        return 'Min is greater than Max; min:\'' . $from . '\'; max:\'' . $to . '\'.';
    }
}
