<?php

namespace Cetria\Laravel\Form\Exception;

use Exception;

class UnsupportedMethodException extends Exception
{
    public function __construct(string $methodName, int $code = 0, $previous = null )
    {
        parent::__construct($this->msg($methodName), $code, $previous);
    }

    private function msg(string $methodName): string
    {
        return 'Deprecated method \'' . $methodName . '\'';
    }
}
