<?php

namespace Cetria\Laravel\Form\Exception;

use InvalidArgumentException;

class ViewDoesNotExistException extends InvalidArgumentException
{
    public function __construct(string $viewName, int $code = 0, $previous = null )
    {
        parent::__construct($this->msg($viewName), $code, $previous);
    }

    private function msg(string $viewName): string
    {
        return 'Undefined view: \'' . $viewName . '\'';
    }
}
