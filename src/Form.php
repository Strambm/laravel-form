<?php

namespace Cetria\Laravel\Form;

use Exception;
use JsonSerializable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Form\HtmlEntities\Entity;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Email;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Enumerable;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;
use Cetria\Laravel\Form\Interfaces\HasEnumerableDescriptionInterface;

/**
 * @see \Cetria\Laravel\Form\Tests\Form\GetContentEntityTest
 * @see \Cetria\Laravel\Form\Tests\Form\HasEntityExistsTest
 */
class Form extends InputBlock
implements JsonSerializable
{
    /** @var FormAttributes */
    protected $params;
    /** @var string */
    protected $view = 'cetria-form::form';

    public function __construct(FormAttributes|null $params = null, array $content = [])
    {
        $this->setParams($params);
        $this->setContent($content);
    }

    private function setParams(FormAttributes|null $params = null): Form
    {
        if(is_null($params)) {
            $params = new FormAttributes();   
        }
        $this->params = $params;
        return $this;
    }

    /**
     * @param array $validators collumn => validator[] or collumn => 'validator1|validator2|validator3'
     * @return static
     * @see \Cetria\Laravel\Form\Tests\Form\ApplyValidatorsTest
     */
    public function applyValidators(array $validators): static
    {
        foreach($validators as $attributeName => $validators) {
            if($this->hasEntityExists($attributeName)) {
                $input = $this->getContentEntity($attributeName);
                foreach(ValidatorTranslator::toRuleCommands($validators) as $command) {
                    $this->applyValidationCommand($input, $command);
                }
                $this->addContent($input);
            }
        }
        return $this;
    }

    private function applyValidationCommand(Input &$attribute, string $command): void
    {
        switch(ValidatorTranslator::getRule($command)) {
            case 'boolean':
                $attribute = Enumerable::makeFromAttribute($attribute);
                $attribute->addOptions([
                    new EnumerableOption(true),
                    new EnumerableOption(false)
                ]);
                break;
            case 'digits_between':
                $attribute = $this->applyValidation__digitsBetween($attribute, ValidatorTranslator::getParameters($command));              
                break;
            case 'email':
                $attribute = Email::makeFromAttribute($attribute);
                break;
            case 'exists':
                $attribute = Enumerable::makeFromAttribute($attribute);
                $attribute->addOptions($this->applyValidation__enumOptionsFromDB(ValidatorTranslator::getParameters($command)));
                break;
            case 'required':
                $attribute->setRequired();
                break;
            case 'numeric':
                $attribute = Number::makeFromAttribute($attribute);
                $attribute->setStep(0.0001, false);
                break;
            case 'integer':
                $attribute = $this->applyValidation__integer($attribute);
                break;
        }
    }

    private function applyValidation__digitsBetween(Input $attribute, array $params): Number
    {
        $attribute = $this->applyValidation__integer($attribute);
        $attribute->setMin(pow(10, $params[0]) /10 - 1);
        $attribute->setMax(pow(10, $params[1]) - 1);
        return $attribute;
    }

    private function applyValidation__enumOptionsFromDB(array $params): array
    {
        $options = [];
        $key = 'id';
        if(array_key_exists(1, $params)) {
            $key = $params[1];
        }
        if(is_subclass_of($params[0], Model::class)) {
            $modelClass = $params[0];
            $items = $modelClass::query()->get();
        } else {
            $items = DB::table($params[0])->distinct()->get();
        }
        foreach($items as $item) {
            if($item instanceof HasEnumerableDescriptionInterface) {
                $options[] = new EnumerableOption($item->$key, $item->getEnumerableDescription());
            } else {
                $options[] = new EnumerableOption($item->$key);
            }
            
        }
        return $options;
    }

    private function applyValidation__integer(Input $attribute): Number
    {
        $attribute = Number::makeFromAttribute($attribute);
        $attribute->setStep(1, false);
        return $attribute;
    }

    /**
     * @return FormAttributes
     * @see \Cetria\Laravel\Form\Tests\Form\GetParamsTest
     */
    public function &getParams(): FormAttributes
    {
        return $this->params;
    } 

    /**
     * @return Entity[]
     * @see \Cetria\Laravel\Form\Tests\Form\GetContentTest
     */
    public function getContent(): array
    {
        return \array_merge(
            parent::getContent(), 
            $this->params->getContent()
        );
    }
}
