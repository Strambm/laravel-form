<?php

namespace Cetria\Laravel\Form\Interfaces;

interface HasEnumerableDescriptionInterface
{
    public function getEnumerableDescription(): string;
}
