<?php

namespace Cetria\Laravel\Form\Interfaces;

use Cetria\Laravel\Form\Form;

/**
 * @see \Cetria\Laravel\Form\Traits\UseCastToForm
 */
interface CastToFormInterface
{
    public function toForm(bool $isUpdate = true): Form;
}
