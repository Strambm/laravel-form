<?php

namespace Cetria\Laravel\Form;

use Exception;
use Illuminate\Support\Str;

class ValidatorTranslator
{
    /**
     * @see \Cetria\Laravel\Form\Tests\ValidatorTranslator\ToRuleCommandsTest
     */
    public static function toRuleCommands(array|string $source): array
    {
        $response = [];
        if(is_array($source)) {
            foreach($source as $row) {
                if(is_string($row)) {
                    $response = array_merge($response, self::toRuleCommands($row));
                }
            }
        } elseif(is_string($source)) {
            $parts = explode('|', $source);
            $response = array_merge($response, $parts);
        }
        $response = array_map(function(string $item): string { return trim($item); }, $response);
        $response = array_unique($response);
        return $response;
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\ValidatorTranslator\GetRuleTest
     */
    public static function getRule(string $commandRule): string
    {
        self::validateCommandRule($commandRule);
        return Str::before($commandRule, ':');
    }

    private static function validateCommandRule(string $commandRule): void
    {
        if(Str::contains($commandRule, '|')) {
            throw new Exception('Command can not contain symbol \'|\'');
        }
    }

    /**
     * @see \Cetria\Laravel\Form\Tests\ValidatorTranslator\GetParametersTest
     */
    public static function getParameters(string $commandRule): array
    {
        self::validateCommandRule($commandRule);
        if(Str::contains($commandRule, ':')) {
            $paramPart = Str::after($commandRule, ':');
            $rawParts = explode(',', $paramPart);
            return self::mappingRawParams($rawParts);
        } else {
            return [];
        }
    }

    private static function mappingRawParams(array $rawParams): array
    {
        return array_merge(...array_map(
            function(string $value): array {
                if(Str::contains($value, '=')) {
                    return [trim(Str::before($value, '=')) => trim(Str::after($value, '='))];
                } else {
                    return [trim($value)];
                }
            }, array_values($rawParams)
        ));
    }
}
