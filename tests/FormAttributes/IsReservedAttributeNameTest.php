<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;
use PHPUnit\Framework\MockObject\MockObject;

class IsReservedAttributeNameTest extends TestCase
{
    #[Test]
    #[DataProvider('isReservedAttributeNameDataProvider')]
    public function isReservedAttributeName(string $name, bool $expectedResult): void
    {
        $instance = $this->getTestModelInstance();
        $result = $instance->isReservedAttributeName($name);
        $this->assertSame($expectedResult, $result);
    }

    public static function isReservedAttributeNameDataProvider(): array
    {
        return [
            [
                '_method',
                false,
            ], [
                'test',
                false,
            ], [
                'param',
                true
            ]
        ];
    }

    protected function getTestModelInstance(): MockObject|FormAttributes
    {
        $instance = $this->getMockBuilder(FormAttributes::class)
            ->onlyMethods(['getReservedAttributeNames'])
            ->disableOriginalConstructor()
            ->getMock();
        $instance->expects($this->any())
            ->method('getReservedAttributeNames')
            ->willReturn([
                'param',
            ]);
        return $instance;
    }
}
