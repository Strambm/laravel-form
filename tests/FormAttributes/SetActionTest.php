<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;
use Cetria\Helpers\Reflection\Reflection;

class SetActionTest extends TestCase
{
    #[Test]
    public function setActionProperty(): void
    {
        $expectedAction = 'gdsg';
        $instance = new FormAttributes();
        $this->assertEquals('', Reflection::getHiddenProperty($instance, 'action'));
        $instance->setAction($expectedAction);
        $this->assertEquals($expectedAction, Reflection::getHiddenProperty($instance, 'action'));
        $this->assertEquals($expectedAction, $instance->getAction());
    }
}
