<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\Attribute\Attribute;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\Attributes\DataProvider;
use Cetria\Laravel\Form\Exception\UndefinedRequestMethodException;

class SetMethodTest extends TestCase
{
    #[Test]
    #[DataProvider('withoutInputDataProvider')]
    public function withoutInput(Method $value): void
    {
        $instance = $this->getInstance();
        $instance->setMethod($value);
        $this->assetEqualMethods($value->name, $instance);
        $this->assertDoesntHaveAttribute($instance);
    }

    public static function withoutInputDataProvider(): array
    {
        return [
            [
                Method::GET
            ], [
                Method::POST
            ],
        ];
    }

    #[Test]
    #[DataProvider('withInputDataProvider')]
    public function withInput(Method $value): void
    {
        $instance = $this->getInstance();
        $instance->setMethod($value);
        $this->assetEqualMethods('POST', $instance);
        $this->assertHasAttribute($value->name, $instance);
    }

    public static function withInputDataProvider(): array
    {
        return [
            [
                Method::DELETE
            ], [
                Method::PATCH
            ], [
                Method::PUT
            ],
        ];
    }

    public function removeMethodAttributeAfterChange(): void
    {
        $instance = $this->getInstance();
        $instance->setMethod(Method::DELETE);
        $instance->setMethod(Method::GET);
        $this->assetEqualMethods(Method::GET->name, $instance);
        $this->assertDoesntHaveAttribute($instance);
    }

    protected function getInstance(): MockObject|FormAttributes
    {
        return $this->getMockBuilder(FormAttributes::class)
            ->onlyMethods([])
            ->disableOriginalConstructor()
            ->getMock();
    }

    private function assetEqualMethods(string $expectedValue, FormAttributes $instance): void
    {
        $this->assertEquals(strtoupper($expectedValue), Reflection::getHiddenProperty($instance, 'method'));
    }

    private function assertDoesntHaveAttribute(FormAttributes $instance): void
    {
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertEquals(0, count($attributes));
    }

    private function assertHasAttribute(string $attributeValue, FormAttributes $instance): void
    {
        /** @var Attribute[] */
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertTrue(array_key_exists('_method', $attributes));
        $this->assertEquals(strtoupper($attributeValue), $attributes['_method']->getValue());
    }
}
