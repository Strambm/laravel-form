<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class IsReservedAttributeTest extends TestCase
{
    #[Test]
    #[DataProvider('isReservedAttributeDataProvider')]
    public function isReservedAttribute(string $attributeName, bool $expectedResult): void
    {
        $testInstance = $this->getTestModelInstance();
        $result = $testInstance->isReservedAttribute(new Input($attributeName));
        $this->assertSame($expectedResult, $result);
    }

    public static function isReservedAttributeDataProvider(): array
    {
        return [
            [
                '_method',
                false,
            ], [
                'test',
                false,
            ], [
                'param',
                true
            ]
        ];
    }

    protected function getTestModelInstance(): MockObject|FormAttributes
    {
        $instance = $this->getMockBuilder(FormAttributes::class)
            ->onlyMethods(['getReservedAttributeNames'])
            ->disableOriginalConstructor()
            ->getMock();
        $instance->expects($this->any())
            ->method('getReservedAttributeNames')
            ->willReturn([
                'param',
            ]);
        return $instance;
    }
}
