<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\Attribute\Hidden;

class ConstructTest extends TestCase
{
    #[Test]
    public function withoutParams(): void
    {
        $instance = new FormAttributes();
        $this->assertEquals('POST', $instance->getMethod());
        $this->assertEquals('', $instance->getAction());
    }

    public function withParams(): void
    {
        $expectedAction = 'efefe';
        $method = Method::PATCH;
        $instance = new FormAttributes($expectedAction, $method);
        $this->assertEquals('POST', Reflection::getHiddenProperty($instance, 'method'));
        $atributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertEquals(1, count($atributes));
        $attribute = current($atributes);
        $this->assertInstanceOf(Hidden::class, $attribute);
        $this->assertEquals('_method', Reflection::getHiddenProperty($attribute, 'name'));
        $this->assertEquals($method->name, Reflection::getHiddenProperty($attribute, 'value'));
    }
}
