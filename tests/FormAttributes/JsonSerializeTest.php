<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;

class JsonSerializeTest extends TestCase
{
    #[Test]
    public function serialize(): void
    {
        $action = 'fakeAction';
        $method = Method::GET;
        $attributes = new FormAttributes($action, $method);
        $serialized = $attributes->jsonSerialize();
        $this->assertEquals($action, $serialized['action']);
        $this->assertEquals($method->name, $serialized['method']);
    }
    
    #[Test]
    public function compareAfterDeserialize(): void
    {
        $action = 'fakeAction';
        $method = Method::GET;
        $attributes = new FormAttributes($action, $method);
        $json = json_encode($attributes);
        $jsonDecoded = json_decode($json, true);
        $this->assertEquals($jsonDecoded, $attributes->jsonSerialize());
    }
}
