<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;

class GetActionTest extends TestCase
{
    #[Test]
    public function getActionProperty(): void
    {
        $expectedValue = 'testst';
        $instance = new FormAttributes($expectedValue);
        $this->assertEquals($expectedValue, $instance->getAction());
    }
}
