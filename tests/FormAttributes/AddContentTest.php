<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use Error;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;
use PHPUnit\Framework\MockObject\MockObject;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\Tests\Traits\HasInputContent\AddContentTest as HasInputContentAddContentTest;

class AddContentTest extends HasInputContentAddContentTest
{
    #[Test]
    public function overide(): void
    {
        $key = '_method';
        /** @var FormAttributes $instance */
        $instance = $this->getTestModelInstance();
        $instance->addContent(new Input($key));
        $instance->addContent(new Input($key));
        $content = $instance->getContent();
        $this->assertCount(1, $content);
        $this->assertArrayHasKey($key, $content);
    }

    #[Test]
    public function instanceOfInputWithKey(): void
    {
        $inputName = '_method';
        $input = new Input($inputName);
        $key = 'blockKey';
        $instance = $this->getTestModelInstance();
        $instance->addContent($input, $key);
        $result = $instance->getContent();
        $this->assertArrayHasKey($inputName, $result);
        $this->assertArrayNotHasKey($key, $result);
    }

    public function instanceOfBlockWithoutKey(): void
    {
        $this->assertTrue(1);
    }

    #[Test]
    public function keyIsNotReservedAttribute(): void
    {
        $this->expectException(Error::class);
        /** @var FormAttributes|MockObject $instance */
        $instance = $this->getMockBuilder(FormAttributes::class)
            ->onlyMethods(['isReservedAttribute'])
            ->disableOriginalConstructor()
            ->getMock();
        $instance->expects($this->any())
            ->method('isReservedAttribute')
            ->willReturn(false);
        $instance->addContent(new Input(''));
    }

    protected function getTestModelInstance(): MockObject|FormAttributes
    {
        $instance = $this->getMockBuilder(FormAttributes::class)
            ->onlyMethods([])
            ->disableOriginalConstructor()
            ->getMock();
        return $instance;
    }
}
