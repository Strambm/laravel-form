<?php

namespace Cetria\Laravel\Form\Tests\FormAttributes;

use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;

class GetMethodTest extends TestCase
{
    #[Test]
    public function getMethodAttribute(): void
    {
        $instance = new FormAttributes('', Method::DELETE);
        $this->assertEquals(Method::POST->name, $instance->getMethod());
    }
}
