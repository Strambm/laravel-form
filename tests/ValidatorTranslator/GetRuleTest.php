<?php

namespace Cetria\Laravel\Form\Tests\ValidatorTranslator;

use Cetria\Laravel\Form\ValidatorTranslator;
use PHPUnit\Framework\TestCase;

class GetRuleTest extends TestCase
{
    public function testMethod__withoutParams(): void
    {
        $source = 'required';
        $response = ValidatorTranslator::getRule($source);
        $this->assertEquals($source, $response);
    }

    public function testMethod__withParams(): void
    {
        $source = 'after:start_date';
        $response = ValidatorTranslator::getRule($source);
        $this->assertEquals('after', $response);
    }
}
