<?php

namespace Cetria\Laravel\Form\Tests\ValidatorTranslator;

use Cetria\Laravel\Form\ValidatorTranslator;
use Illuminate\Validation\Rules\RequiredIf;
use PHPUnit\Framework\TestCase;

class ToRuleCommandsTest extends TestCase
{
    public function testMethod__multipleString(): void
    {
        $source = 'required| date |after:tomorrow ';
        $response = ValidatorTranslator::toRuleCommands($source);
        $this->assertCount(3, $response);
        $this->assertContains('required', $response);
        $this->assertContains('date', $response);
        $this->assertContains('after:tomorrow', $response);
    }

    public function testMethod__withArray__objectInArray(): void
    {
        $source = [
            'required',
            'date',
            new RequiredIf(false),
        ];
        $response = ValidatorTranslator::toRuleCommands($source);
        $this->assertCount(2, $response);
        $this->assertContains('required', $response);
        $this->assertContains('date', $response);
    }

    public function testMethod__withDuplication(): void
    {
        $source = [
            'required|date',
            'date',
        ];
        $response = ValidatorTranslator::toRuleCommands($source);
        $this->assertCount(2, $response);
        $this->assertContains('required', $response);
        $this->assertContains('date', $response);
    }
}
