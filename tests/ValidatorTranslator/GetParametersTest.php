<?php

namespace Cetria\Laravel\Form\Tests\ValidatorTranslator;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\ValidatorTranslator;

class GetParametersTest extends TestCase
{
    #[Test]
    public function withoutParams(): void
    {
        $source = 'required';
        $response = ValidatorTranslator::getParameters($source);
        $this->assertCount(0, $response);
    }

    #[Test]
    public function withParamsWithoutKey(): void
    {
        $source = 'after:start_date';
        $response = ValidatorTranslator::getParameters($source);
        $this->assertCount(1, $response);
        $this->assertEquals($response[0], 'start_date');
    }

    #[Test]
    public function withParamsWithKey(): void
    {
        $source = 'dimensions:min_width = 100,min_height=200';
        $response = ValidatorTranslator::getParameters($source);
        $this->assertCount(2, $response);
        $this->assertArrayHasKey('min_width', $response);
        $this->assertArrayHasKey('min_height', $response);
        $this->assertEquals('100', $response['min_width']);
        $this->assertEquals('200', $response['min_height']);
    }
}
