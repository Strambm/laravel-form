<?php

namespace Cetria\Laravel\Form\Tests\Form;

use Cetria\Laravel\Form\Form;
use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock\GetContentTest as InputBlockGetContentTest;

class GetContentTest extends InputBlockGetContentTest
{
    #[Test]
    public function mergeWithParams(): void
    {
        $instance = $this->getTestInstance();
        $basicAttributes = [
            'key' => new Input('key', 8),
            'temp' => new Input('temp', 454),
        ];
        $formParamsAttributes = [
            '_method' => new Input('_method', Method::DELETE->name)
        ];
        Reflection::setHiddenProperty($instance, 'content', $basicAttributes);
        $params = Reflection::getHiddenProperty($instance, 'params');
        Reflection::setHiddenProperty($params, 'content', $formParamsAttributes);
        Reflection::setHiddenProperty($instance, 'params', $params);
        $expected = array_merge($basicAttributes, $formParamsAttributes);
        $stored = $instance->getContent();
        $this->assertEquals(count($expected), count($stored));
        foreach(array_keys($stored) as $key) {
            $this->assertEquals($expected[$key], $stored[$key]);
        }
    }

    protected function getTestClass(): string
    {
        return Form::class;
    }
}
