<?php

namespace Cetria\Laravel\Form\Tests\Form;

use Cetria\Laravel\Form\Form;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\FormAttributes;
use Cetria\Helpers\Reflection\Reflection;

class GetParamsTest extends TestCase
{
    #[Test]
    public function reference(): void
    {
        $fakeParams = 'fakeParam';
        $instance = new Form();
        $params = $instance->getParams();
        $this->assertInstanceOf(FormAttributes::class, $params);
        Reflection::setHiddenProperty($params, 'action', $fakeParams);
        $params = $instance->getParams();
        $storedAction = Reflection::getHiddenProperty($params, 'action');
        $this->assertEquals($fakeParams, $storedAction);
    }

    
}
