<?php

namespace Cetria\Laravel\Form\Tests\Form;

use Cetria\Laravel\Form\Form;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Helpers\NumberHelper\NumberHelper;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Email;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Enumerable;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;
use Cetria\Laravel\Helpers\Test\Dummy\ProductWithGlobalScope;
use Cetria\Laravel\Form\Interfaces\HasEnumerableDescriptionInterface;

class ApplyValidatorsTest extends TestCase
{
    #[Test]
    public function required(): void
    {
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey),
        ];
        $validators = [
            $attributeKey => 'required',
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertTrue(Reflection::getHiddenProperty($attributes[$attributeKey], 'isRequired'));
    }

    #[Test]
    public function integer(): void
    {
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey),
        ];
        $validators = [
            $attributeKey => 'integer',
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertInstanceOf(Number::class, $attributes[$attributeKey]);
        $this->assertEquals(0, NumberHelper::decimals($attributes[$attributeKey]->getStep()));
    }

    #[Test]
    public function numeric(): void
    {
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey),
        ];
        $validators = [
            $attributeKey => 'numeric',
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertInstanceOf(Number::class, $attributes[$attributeKey]);
    }

    #[Test]
    public function email(): void
    {
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey),
        ];
        $validators = [
            $attributeKey => 'email:rfc,dns',
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertInstanceOf(Email::class, $attributes[$attributeKey]);
    }

    #[Test]
    public function boolean(): void
    {
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey),
        ];
        $validators = [
            $attributeKey => 'boolean',
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertInstanceOf(Enumerable::class, $attributes[$attributeKey]);
        $options = $attributes[$attributeKey]->getOptions();
        $this->assertEquals(2, count($options));
        $this->assertEquals(1, count(array_filter($options, function(EnumerableOption $option): bool { return $option->getValue() == true; })));
        $this->assertEquals(1, count(array_filter($options, function(EnumerableOption $option): bool { return $option->getValue() == false; })));
    }

    #[Test]
    public function existsModel(): void
    {
        DummyHelper::init();
        $itemsCount = 5;
        $products = Product::factory()->count(5)->create();
        $prodInstance = new Product();
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey),
        ];
        $validators = [
            $attributeKey => 'exists:' . Product::class,
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertInstanceOf(Enumerable::class, $attributes[$attributeKey]);
        $options = $attributes[$attributeKey]->getOptions();
        $this->assertCount($itemsCount, $options);
        foreach($options as $option) {
            $this->assertCount(1, $products->where($prodInstance->getKeyName(), '=', $option->getValue()));
        }
    }

    #[Test]
    public function existsModelColumn(): void
    {
        DummyHelper::init();
        $itemsCount = 5;
        $products = Product::factory()->uniqueName()->count($itemsCount)->create();
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey),
        ];
        $validators = [
            $attributeKey => 'exists:' . Product::class . ', name',
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertInstanceOf(Enumerable::class, $attributes[$attributeKey]);
        $options = $attributes[$attributeKey]->getOptions();
        $this->assertCount($itemsCount, $options);
        foreach($options as $option) {
            $this->assertCount(1, $products->where('name', '=', $option->getValue()));
        }
    }

    #[Test]
    public function existsModelDescription(): void
    {
        $class = new class extends Product implements HasEnumerableDescriptionInterface
        {
            static $tmp = 1;

            public function getEnumerableDescription(): string
            {
                return 'custom_label' . $this->id;
            }
        };
        DummyHelper::init();
        $itemsCount = 5;
        $products = $class::factory()->count($itemsCount)->create();
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey, 4),
        ];
        $validators = [
            $attributeKey => 'exists:' . get_class($class),
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertInstanceOf(Enumerable::class, $attributes[$attributeKey]);
        $options = $attributes[$attributeKey]->getOptions();
        $this->assertCount($itemsCount, $options);
        foreach($options as $option) {
            $this->assertEquals($option->getDescription(), 'custom_label' . $option->getValue());
        }
    }

    #[Test]
    public function existModelWithGlobalScope(): void
    {
        $class = new ProductWithGlobalScope();
        DummyHelper::init();
        $products = ProductWithGlobalScope::factory()->count(5)->create(['price' => 5]);
        $fakeProducts = ProductWithGlobalScope::factory()->count(5)->create(['price' => 1000]);
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey, 4),
        ];
        $validators = [
            $attributeKey => 'exists:' . get_class($class),
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $this->assertInstanceOf(Enumerable::class, $attributes[$attributeKey]);
        $options = $attributes[$attributeKey]->getOptions();
        $this->assertCount($products->count(), $options);
        $this->assertEquals($products->count() + $fakeProducts->count(), ProductWithGlobalScope::withoutGLobalScope('testGlobalScope')->count());
    }

    #[Test]
    public function between(): void
    {
        $attributeKey = 'test';
        $attributes = [
            $attributeKey => new Input($attributeKey, 4),
        ];
        $validators = [
            $attributeKey => 'digits_between:1,3',
        ];
        $instance = new Form();
        Reflection::setHiddenProperty($instance, 'content', $attributes);
        $instance->applyValidators($validators);
        $attributes = Reflection::getHiddenProperty($instance, 'content');
        $attribute = $attributes[$attributeKey];
        $this->assertInstanceOf(Number::class, $attribute);
        $this->assertEquals(0, Reflection::getHiddenProperty($attribute, 'min'));
        $this->assertEquals(999, Reflection::getHiddenProperty($attribute, 'max'));
    }
}
