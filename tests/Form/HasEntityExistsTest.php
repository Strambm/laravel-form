<?php

namespace Cetria\Laravel\Form\Tests\Form;

use Cetria\Laravel\Form\Form;
use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock\HasEntityExistsTest as InputBlockHasEntityExistsTest;

class HasEntityExistsTest extends InputBlockHasEntityExistsTest
{
    #[Test]
    public function entityExistsInParams(): void
    {
        /** @var Form $instance */
        $instance = $this->getTestInstance();
        $instance->getParams()->setMethod(Method::PATCH);
        $this->assertTrue($instance->hasEntityExists('_method'));
    }

    protected function getTestClass(): string
    {
        return Form::class;
    }
}
