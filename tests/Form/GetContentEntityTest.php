<?php

namespace Cetria\Laravel\Form\Tests\Form;

use Cetria\Laravel\Form\Form;
use Cetria\Laravel\Form\Method;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock\GetContentEntityTest as InputBlockGetContentEntityTest;

class GetContentEntityTest extends InputBlockGetContentEntityTest
{
    #[Test]
    public function entityInParams(): void
    {
        /** @var Form $form */
        $form = $this->getTestInstance();
        $form->getParams()->setMethod(Method::DELETE);
        /** @var Input $input */
        $input = $form->getContentEntity('_method');
        $this->assertEquals('DELETE', $input->getValue());
    }

    protected function getTestClass(): string
    {
        return Form::class;
    }
}
