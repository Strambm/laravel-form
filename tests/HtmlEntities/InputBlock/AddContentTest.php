<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class AddContentTest extends TestCase
{
    #[Test]
    public function overide(): void
    {
        $key = 'key';
        $block = $this->newTestInstance();
        $block->addContent((new InputBlock())->setNamePrefix($key));
        $block->addContent((new InputBlock())->setNamePrefix($key));
        $content = $block->getContent();
        $this->assertCount(1, $content);
        $this->assertArrayHasKey($key, $content);
    }

    #[Test]
    public function instanceOfInputWithKey(): void
    {
        $inputName = 'inputName';
        $input = new Input($inputName);
        $key = 'blockKey';
        $block = $this->newTestInstance();
        $block->addContent($input, $key);
        $result = $block->getContent();
        $this->assertArrayHasKey($inputName, $result);
        $this->assertArrayNotHasKey($key, $result);
    }

    protected function newTestInstance(mixed ...$arguments): InputBlock
    {
        $class = $this->testClass();
        return new $class(...$arguments);
    }

    protected function testClass(): string
    {
        return InputBlock::class;
    }
}
