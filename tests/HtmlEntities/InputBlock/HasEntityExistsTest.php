<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class HasEntityExistsTest extends TestCase
{
    #[Test]
    public function hasEntityExists(): void
    {
        $block = $this->getTestInstance();
        $block->addContent(new Input('test'));
        $this->assertTrue($block->hasEntityExists('test'));
        $this->assertFalse($block->hasEntityExists('test1'));
    }

    #[Test]
    public function hasEntityExistsRecursive(): void
    {
        $form = $this->getTestInstance();
        $inputBlock = new InputBlock();
        $input = new Input('test');
        $inputBlock->addContent($input);
        $form->addContent($inputBlock, 'block');
        $this->assertTrue($form->hasEntityExists('test'));
    }

    protected function getTestClass(): string
    {
        return InputBlock::class;
    }

    protected function getTestInstance(): InputBlock
    {
        $class = $this->getTestClass();
        return new $class();
    }
}
