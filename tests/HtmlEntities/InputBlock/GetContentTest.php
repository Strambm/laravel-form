<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class GetContentTest extends TestCase
{
    #[Test]
    public function empty(): void
    {
        $block = $this->getTestInstance();
        $this->assertCount(0, $block->getContent());
    }

    #[Test]
    public function basic(): void
    {
        $block = $this->getTestInstance();
        $block->addContent(new Input('param1'));
        $block->addContent(new Input('param2'));
        $block->addContent(new Input('param3'));
        $content = $block->getContent();
        $this->assertCount(3, $content);
        $this->assertArrayHasKey('param1', $content);
        $this->assertArrayHasKey('param2', $content);
        $this->assertArrayHasKey('param3', $content);
    }

    protected function getTestInstance(): InputBlock
    {
        $class = $this->getTestClass();
        return new $class();
    }

    protected function getTestClass(): string
    {
        return InputBlock::class;
    }
}
