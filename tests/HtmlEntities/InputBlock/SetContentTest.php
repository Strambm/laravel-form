<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock;

use TypeError;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class SetContentTest extends TestCase
{
    #[Test]
    public function overide(): void
    {
        $block = new InputBlock();
        $block->addContent(new Input('before'));
        $block->setContent([
            new Input('After')
        ]);
        $this->assertCount(1, $block->getContent());
        $this->assertArrayHasKey('After', $block->getContent());
    }

    #[Test]
    public function fakeKeys(): void
    {
        $block = new InputBlock();
        $block->setContent([
            'test' => new Input('After')
        ]);
        $this->assertCount(1, $block->getContent());
        $this->assertArrayHasKey('After', $block->getContent());
    }

    #[Test]
    public function badInputFormat(): void
    {
        $this->expectException(TypeError::class);
        $block = new InputBlock();
        $block->setContent([
            'test' => 15
        ]);
    }
}
