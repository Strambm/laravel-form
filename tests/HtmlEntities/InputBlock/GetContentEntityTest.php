<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\InputBlock;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\Exception\AttributeDoesNotExistException;

class GetContentEntityTest extends TestCase
{
    #[Test]
    public function unexistsKey(): void
    {
        $this->expectException(AttributeDoesNotExistException::class);
        $block = $this->getTestInstance();
        $block->getContentEntity('test');
    }

    #[Test]
    public function getContentEntity(): void
    {
        $block = $this->getTestInstance();
        $input1 = new Input('input1');
        $block->addContent(new Input('noise'));
        $block->addContent($input1);
        $result = $block->getContentEntity('input1');
        $this->assertEquals($input1, $result);
    }

    protected function getTestInstance(): InputBlock
    {
        $class = $this->getTestClass();
        return new $class();
    }

    protected function getTestClass(): string
    {
        return InputBlock::class;
    }
}
