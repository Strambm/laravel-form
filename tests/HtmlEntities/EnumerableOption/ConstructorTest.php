<?php

namespace Cetria\Laravel\Form\Tests\Attribute\EnumerableOption;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;

class ConstructorTest extends TestCase
{
    public function testMethod(): void
    {
        $value = 1212;
        $description = 'value';
        $instance = new EnumerableOption($value, $description);
        $storedValue = Reflection::getHiddenProperty($instance, 'value');
        $storedDescription = Reflection::getHiddenProperty($instance, 'description');
        $this->assertEquals($value, $storedValue);
        $this->assertEquals($description, $storedDescription);
    }

    public function testMethod__default(): void
    {
        $value = 1212;
        $instance = new EnumerableOption($value);
        $storedValue = Reflection::getHiddenProperty($instance, 'value');
        $storedDescription = Reflection::getHiddenProperty($instance, 'description');
        $this->assertEquals($value, $storedValue);
        $this->assertNull($storedDescription);
    }
}
