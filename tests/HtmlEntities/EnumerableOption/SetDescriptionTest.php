<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;

class SetDescriptionTest extends TestCase
{
    #[Test]
    public function setDescriptionProperty(): void
    {
        $expectedValue = 'newValue';
        $instance = new EnumerableOption(15);
        $instance->setDescription($expectedValue);
        $this->assertEquals($expectedValue, Reflection::getHiddenProperty($instance, 'description'));
        $this->assertEquals($expectedValue, $instance->getDescription());
    }
}
