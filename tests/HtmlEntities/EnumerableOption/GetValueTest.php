<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;

class GetValueTest extends TestCase
{
    #[Test]
    public function getValueProperty(): void
    {
        $expectedValue = 'newValue';
        $instance = new EnumerableOption(15);
        Reflection::setHiddenProperty($instance, 'value', $expectedValue);
        $this->assertEquals($expectedValue, $instance->getValue());
    }
}
