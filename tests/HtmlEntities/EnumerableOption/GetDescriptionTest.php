<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;

class GetDescriptionTest extends TestCase
{
    #[Test]
    public function hasDescription(): void
    {
        $expectedValue = 'newValue';
        $instance = new EnumerableOption(15);
        $instance->setDescription($expectedValue);
        $this->assertEquals($expectedValue, $instance->getDescription());
    }

    #[Test]
    public function hasNotDescription(): void
    {
        $instance = new EnumerableOption(15);
        $this->assertEquals(15, $instance->getDescription());
    }
}
