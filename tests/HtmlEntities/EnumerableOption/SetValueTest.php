<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\EnumerableOption;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;

class SetValueTest extends TestCase
{
    public function testMethod(): void
    {
        $expectedValue = 'newValue';
        $instance = new EnumerableOption(15);
        $instance->setValue($expectedValue);
        $this->assertEquals($expectedValue, Reflection::getHiddenProperty($instance, 'value'));
        $this->assertEquals($expectedValue, $instance->getValue());
    }
}
