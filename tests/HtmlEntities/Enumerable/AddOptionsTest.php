<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Enumerable;

use TypeError;
use function count;
use function current;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Enumerable;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;

class AddOptionsTest extends TestCase
{   
    #[Test]
    public function addOptionsProperty(): void
    {
        $option = new EnumerableOption(1, 'testVal');
        $instance = new Enumerable('test');
        $instance->addOptions([$option]);
        $options = Reflection::getHiddenProperty($instance, 'options');
        $this->assertEquals(1, count($options));
        $this->assertEquals($option, current($options));
    }

    #[Test]
    public function empty(): void
    {
        $instance = new Enumerable('test');
        Reflection::setHiddenProperty($instance, 'options', [new EnumerableOption(1, 'testVal')]);
        $instance->addOptions([]);
        $options = Reflection::getHiddenProperty($instance, 'options');
        $this->assertEquals(0, count($options));
    }

    #[Test]
    public function badValue(): void
    {
        $this->expectException(TypeError::class);
        $instance = new Enumerable('test');
        $instance->addOptions([12]);
    }
}
