<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Enumerable;

use function count;
use function current;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Enumerable;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;

class AddOptionTest extends TestCase
{   
    #[Test]
    public function addOptionProperty(): void
    {
        $option = new EnumerableOption(1, 'testVal');
        $instance = new Enumerable('test', 'test');
        $instance->addOption($option);
        $options = Reflection::getHiddenProperty($instance, 'options');
        $this->assertEquals(1, count($options));
        $this->assertEquals($option, current($options));
    }

    #[Test]
    public function sameDescription(): void
    {
        $description = 'sameDescription';
        $instance = new Enumerable('test', 'test');
        $instance->addOption(new EnumerableOption(1, $description));
        $instance->addOption(new EnumerableOption(2, $description));
        $options = Reflection::getHiddenProperty($instance, 'options');
        $this->assertEquals(2, count($options));
    }

    #[Test]
    public function sameValue(): void
    {
        $value = 8;
        $instance = new Enumerable('test', 'test');
        $instance->addOption(new EnumerableOption($value, 'desc1'));
        $expectedOption = new EnumerableOption($value, 'desc2');
        $instance->addOption($expectedOption);
        $options = Reflection::getHiddenProperty($instance, 'options');
        $this->assertEquals(1, count($options));
        $this->assertEquals($expectedOption, current($options));
    }
}
