<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Enumerable;

use function count;
use function current;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Enumerable;
use Cetria\Laravel\Form\HtmlEntities\Inputs\EnumerableOption;

class GetOptionsTest extends TestCase
{   
    #[Test]
    public function getOptionsProperty(): void
    {
        $option = new EnumerableOption(1, 'testVal');
        $instance = new Enumerable('test', 'test');
        Reflection::setHiddenProperty($instance, 'options', [$option]);
        $options = $instance->getOptions();
        $this->assertEquals(1, count($options));
        $this->assertEquals($option, current($options));
    }
}
