<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Entity;

use Cetria\Laravel\Form\HtmlEntities\Entity;
use PHPUnit\Framework\TestCase as ParentTestCase;

class TestCase extends ParentTestCase
{
    protected function getEntityInstance(): Entity
    {
        $class = new class() extends Entity
        {
            public function changeNamePrefix(string $from, string $to): static
            {
                throw new \Exception('Unimplemented method');
            }
        };
        return $class;
    }
}
