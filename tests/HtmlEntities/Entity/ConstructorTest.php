<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Entity;

use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Entity;

class ConstructorTest extends TestCase
{
    #[Test]
    public function idIncrementTest(): void
    {
        $idBefore = Reflection::getHiddenProperty(Entity::class, 'idCounter');
        $entity1 = $this->getEntityInstance();
        $entity2 = $this->getEntityInstance();

        $this->assertEquals($idBefore + 1, Reflection::getHiddenProperty($entity1, 'id'));
        $this->assertEquals($idBefore + 2, Reflection::getHiddenProperty($entity2, 'id'));
    }  
}
