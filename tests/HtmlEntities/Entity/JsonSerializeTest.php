<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Entity;

use PHPUnit\Framework\Attributes\Test;

class JsonSerializeTest extends TestCase
{
    #[Test]
    public function serialize(): void
    {
        $entity = $this->getEntityInstance();
        $serialized = $entity->jsonSerialize();
        $this->assertArrayNotHasKey('idCounter', $serialized);
        $this->assertArrayNotHasKey('view', $serialized);
    }
    
    #[Test]
    public function compareAfterDeserialize(): void
    {
        $entity = $this->getEntityInstance();
        $json = json_encode($entity);
        $jsonDecoded = json_decode($json, true);
        $this->assertEquals($jsonDecoded, $entity->jsonSerialize());
    }
}
