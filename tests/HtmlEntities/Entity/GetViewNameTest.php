<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Entity;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;

class GetViewNameTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $instance = $this->getEntityInstance();
        $newVal = 'idk';
        Reflection::setHiddenProperty($instance, 'view', $newVal);
        $this->assertEquals($newVal, $instance->getViewName());
    }
}
