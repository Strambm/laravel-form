<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Entity;

use Illuminate\Support\Facades\View;
use Illuminate\Contracts\View\Factory;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\Exception\ViewDoesNotExistException;

class SetViewNameTest extends TestCase
{
    #[Test]
    public function correct(): void
    {
        $expectedViewRoute = 'fake';
        $this->mockFacade(true);
        $entity = $this->getEntityInstance();
        $entity->setViewName($expectedViewRoute);
        $storedView = Reflection::getHiddenProperty($entity, 'view');
        $this->assertEquals($expectedViewRoute, $storedView);
    }

    #[Test]
    public function unexistsBladeName(): void
    {
        $this->expectException(ViewDoesNotExistException::class);
        $expectedViewRoute = 'fake';
        $this->mockFacade(false);
        $entity = $this->getEntityInstance();
        $entity->setViewName($expectedViewRoute);
    }

    protected function mockFacade(bool $expectedResult): void
    {
        $mockedView = $this->getMockBuilder(Factory::class)
            ->getMock();
        $mockedView->expects($this->once())
            ->method('exists')
            ->with('fake')
            ->willReturn($expectedResult);
        View::swap($mockedView);
    }
}
