<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Entity;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;

class GetIdTest extends TestCase
{
    #[Test]
    public function idIncrementTest(): void
    {
        $entity = $this->getEntityInstance();
        $entityId = Reflection::getHiddenProperty($entity, 'id');
        $this->assertEquals('cetria-form-' . $entityId, $entity->getId());
    }  

    #[Test]
    public function unique(): void
    {
        $entity1 = $this->getEntityInstance();
        $entity2 = $this->getEntityInstance();

        $this->assertNotEquals($entity1->getId(), $entity2->getId());
    }
}
