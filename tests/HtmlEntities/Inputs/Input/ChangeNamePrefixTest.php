<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class ChangeNamePrefixTest extends TestCase
{
    #[Test]
    public function rename(): void
    {
        $value1 = 'v1';
        $value2 = 'v2';
        $value3 = 'v3';
        $input = new Input($value1);
        $input->setName($value1 . $value2);
        $input->changeNamePrefix($value1, $value3);
        $this->assertEquals($value3.$value2, $input->getName());
    }
}
