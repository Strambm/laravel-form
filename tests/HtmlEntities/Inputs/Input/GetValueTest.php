<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class GetValueTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $expectedValue = 'myValue';
        $instance = new Input('test', 'test');
        Reflection::setHiddenProperty($instance, 'value', $expectedValue);
        $this->assertEquals($expectedValue, $instance->getValue());
    }
}
