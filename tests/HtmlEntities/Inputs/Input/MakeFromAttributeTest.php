<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class MakeFromAttributeTest extends TestCase
{   
    #[Test]
    public function differentInstanceOfInput(): void
    {
        $source = $this->arrangeDifferentInstanceOfInput();
        $response = Input::makeFromAttribute($source);
        $this->assertEquals(Input::class, get_class($response));
        $this->assertCompareAttributes($source, $response, ['name', 'value', 'isRequired']);
    }

    #[Test]
    public function testMethod__sameClass(): void
    {
        $source = new Input('test', 1);
        $response = Input::makeFromAttribute($source);
        $this->assertEquals($source, $response);
    }

    protected function arrangeDifferentInstanceOfInput(): Input
    {
        $class = new class('name', 'value') extends Input
        {

        };
        return $class;
    }

    protected function assertCompareAttributes(Input $source, Input $response, array $params): void
    {
        foreach($params as $param) {
            $sourceParam = Reflection::getHiddenProperty($source, $param);
            $responseParam = Reflection::getHiddenProperty($response, $param);
            $this->assertEquals($sourceParam, $responseParam);
        }
    }
}
