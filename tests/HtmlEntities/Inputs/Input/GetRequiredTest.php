<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class GetRequiredTest extends TestCase
{
    #[Test]
    public function default(): void
    {
        $instance = new Input('test', 'test');
        $this->assertFalse($instance->getRequired());
    }

    #[Test]
    public function getRequired(): void
    {
        $instance = new Input('test', 'test');
        Reflection::setHiddenProperty($instance, 'isRequired', true);
        $this->assertTrue($instance->getRequired());
    }
}
