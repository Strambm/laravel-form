<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class SetNameTest extends TestCase
{
    #[Test]
    public function setNameProperty(): void
    {
        $instance = new Input('name');
        $instance->setName('newName');
        $this->assertEquals('newName', Reflection::getHiddenProperty($instance, 'name'));
        $this->assertEquals('newName', $instance->getName());
    }
}
