<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class HasLabelTest extends TestCase
{
    #[Test]
    public function withoutLabel(): void
    {
        $instance = new Input('test', 5);
        $this->assertFalse($instance->hasLabel());
    }

    #[Test]
    public function withLabel(): void
    {
        $instance = new Input('test', 5);
        $instance->setLabel('label');
        $this->assertTrue($instance->hasLabel());
    }
}
