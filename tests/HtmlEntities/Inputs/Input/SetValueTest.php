<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use Error;
use stdClass;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\DataProvider;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class SetValueTest extends TestCase
{
    #[Test]
    #[DataProvider('setValueDataProvider')]
    public function correct(mixed $value): void
    {
        $instance = $this->makeInstace('name');
        $instance->setValue($value);
        $this->assertEquals((string) $value, Reflection::getHiddenProperty($instance, 'value'));
        $this->assertEquals((string) $value, $instance->getValue());
    }

    public static function setValueDataProvider(): array
    {
        return [
            [
                15
            ], [
                null
            ], [
                'test'
            ], [
                15.5
            ]
        ];
    } 

    #[Test]
    #[DataProvider('badInputDataProvider')]
    public function badInput(mixed $value): void
    {
        $this->expectException(Error::class);
        $instance = $this->makeInstace('name');
        $instance->setValue($value);
    }

    public static function badInputDataProvider(): array
    {
        return [
            [
                [
                    'val'
                ]
            ], [
                new stdClass
            ]
        ];
    }

    protected function getTestObjectClass(): string
    {
        return Input::class;
    }

    protected function makeInstace(mixed ...$arguments): Input
    {
        $class = $this->getTestObjectClass();
        return new $class(...$arguments);
    }
}
