<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class GetInputTypeTest extends TestCase
{   
    #[Test]
    public function testMethod(): void
    {
        $instance = new Input('test', 'test');
        $this->assertEqualsIgnoringCase('text', $instance->getInputTypeAttribute());
    }
}
