<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class GetNameTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $expectedName = 'myName';
        $instance = new Input('test', 'test');
        Reflection::setHiddenProperty($instance, 'name', $expectedName);
        $this->assertEquals($expectedName, $instance->getName());
    }
}
