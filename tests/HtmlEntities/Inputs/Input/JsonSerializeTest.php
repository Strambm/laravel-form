<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\Tests\HtmlEntities\Entity\jsonSerializeTest as EntityJsonSerializeTest;

class JsonSerializeTest extends EntityJsonSerializeTest
{
    #[Test]
    public function serialize(): void
    {
        $name = 'tmpName';
        $value = 154;
        $entity = new Input($name, $value);
        $serialized = $entity->jsonSerialize();
        $this->assertEquals($name, $serialized['name']);
        $this->assertEquals($value, $serialized['value']);
        $this->assertEquals($entity->getInputTypeAttribute(), $serialized['type']);
        $this->assertArrayNotHasKey('idCounter', $serialized);
        $this->assertArrayNotHasKey('view', $serialized);
    }
    
    #[Test]
    public function compareAfterDeserialize(): void
    {
        $name = 'tmpName';
        $value = 154;
        $entity = new Input($name, $value);
        $json = json_encode($entity);
        $jsonDecoded = json_decode($json, true);
        $this->assertEquals($jsonDecoded, $entity->jsonSerialize());
    }
}
