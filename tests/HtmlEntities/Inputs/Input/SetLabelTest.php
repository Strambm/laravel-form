<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\DataProvider;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class SetLabelTest extends TestCase
{
    #[Test]
    #[DataProvider('setLabelDataProvider')]
    public function setLabel(string|null $value): void
    {
        $instance = new Input('test', 5);
        $instance->setLabel($value);
        $this->assertEquals($value, Reflection::getHiddenProperty($instance, 'label'));
    }

    public static function setLabelDataProvider(): array
    {
        return [
            [
                null
            ], [
                'nextLabel'
            ]
        ];
    }
}
