<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class SetRequiredTest extends TestCase
{
    #[Test]
    public function default(): void
    {
        $instance = new Input('test', 'test');
        Reflection::setHiddenProperty($instance, 'isRequired', false);
        $instance->setRequired();
        $this->assertTrue(Reflection::getHiddenProperty($instance, 'isRequired'));
    }

    #[Test]
    public function setRequired(): void
    {
        $instance = new Input('test', 'test');
        Reflection::setHiddenProperty($instance, 'isRequired', true);
        $instance->setRequired(false);
        $this->assertFalse(Reflection::getHiddenProperty($instance, 'isRequired'));
    }
}
