<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Entity;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\Tests\HtmlEntities\Entity\ConstructorTest as EntityConstructorTest;

class ConstructorTest extends EntityConstructorTest
{
    #[Test]
    public function idIncrementTest(): void
    {
        $idBefore = Reflection::getHiddenProperty(Entity::class, 'idCounter');
        $entity1 = $this->makeInstance('name');
        $entity2 = $this->makeInstance('name');

        $this->assertEquals($idBefore + 1, Reflection::getHiddenProperty($entity1, 'id'));
        $this->assertEquals($idBefore + 2, Reflection::getHiddenProperty($entity2, 'id'));
        $this->assertEquals($idBefore + 2, Reflection::getHiddenProperty(Entity::class, 'idCounter'));
    } 

    #[Test]
    public function setNameParameter(): void
    {
        $name = Str::random(5);
        $entity = $this->makeInstance($name);
        $this->assertEquals($name, Reflection::getHiddenProperty($entity, 'name'));
        $this->assertEquals($name, $entity->getName());
    }

    #[Test]
    #[DataProvider('setValueParameterDataProvider')]
    public function setValueParameter(mixed $value): void
    {
        $entity = $this->makeInstance('name', $value);
        $this->assertEquals($value, Reflection::getHiddenProperty($entity, 'value'));
        $this->assertEquals($value, $entity->getValue());
    }

    public static function setValueParameterDataProvider(): array
    {
        return [
            [
                15,
            ], [
                45.5
            ], [
                'testValue',
            ],
        ];
    }

    protected function makeInstance(mixed ...$arguments): Input
    {
        $class = static::getTestObjectClass();
        return new $class(...$arguments);
    }

    protected function getTestObjectClass(): string
    {
        return Input::class;
    }
}
