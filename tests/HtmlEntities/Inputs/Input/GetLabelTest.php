<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\DataProvider;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class GetLabelTest extends TestCase
{
    #[Test]
    #[DataProvider('getLabelDataProvider')]
    public function getLabel(string|null $expectedLabel): void
    {
        $instance = new Input('test', 5);
        Reflection::setHiddenProperty($instance, 'label', $expectedLabel);
        $this->assertEquals((string) $expectedLabel, $instance->getLabel());
    }

    public static function getLabelDataProvider(): array
    {
        return [
            [
                null
            ], [
                'label'
            ]
        ];
    }
}
