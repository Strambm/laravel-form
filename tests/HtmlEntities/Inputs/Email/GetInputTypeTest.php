<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Email;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Email;
use Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetInputTypeTest as InputGetInputTypeTest;

class GetInputTypeTest extends InputGetInputTypeTest
{   
    #[Test]
    public function testMethod(): void
    {
        $instance = new Email('test', 'test');
        $this->assertEqualsIgnoringCase('email', $instance->getInputTypeAttribute());
    }
}
