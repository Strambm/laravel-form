<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;

class GetStepTest extends TestCase
{
    #[Test]
    #[DataProvider('getStepDataProvider')]
    public function withSetStep(float $step, string $expectedStep): void
    {
        $instance = new Number('test');
        $instance->setStep($step);
        $this->assertEquals($expectedStep, $instance->getStep());
    }

    public static function getStepDataProvider(): array
    {
        return [
            [
                1,
                '1'
            ], [
                0.555,
                '0.555'
            ], [
                152.555,
                '152.555'
            ]
        ];
    }

    #[Test]
    public function withoutSetStep(): void
    {
        $instance = new Number('test');
        $this->assertEquals(1, $instance->getStep());
    }
}
