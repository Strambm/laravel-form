<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;

class GetMaxTest extends TestCase
{
    public function testMethod(): void
    {
        $instance = new Number('test');
        Reflection::setHiddenProperty($instance, 'max', 55);
        $this->assertEquals(55, $instance->getMax());
    }
}
