<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use function rand;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use Cetria\Laravel\Form\Exception\BadNumberBetweenException;

class SetMaxTest extends TestCase
{
    #[Test]
    public function setMaxProperty(): void
    {
        $value = rand(-999999999, 999999999999);
        $instance = $this->newInstance();
        $instance->setMax($value);
        $this->assertEquals($value, Reflection::getHiddenProperty($instance, 'max'));
    }

    #[Test]
    public function valueLowerThanMin(): void
    {
        $this->expectException(BadNumberBetweenException::class);
        $instance = $this->newInstance(1);
        $instance->setMax(-1);
    }

    #[Test]
    public function valueGreaterThanStored(): void
    {
        $instance = $this->newInstance(null, 1);
        $instance->setMax(5);
        $this->assertEquals(5, $instance->getMax());
    }

    #[Test]
    public function valueGreaterThanStoredWithAbsolute(): void
    {
        $instance = $this->newInstance(5);
        $instance->setMax(6, true);
        $this->assertEquals(6, $instance->getMax());
    }

    #[Test]
    public function valueLowerThanStored(): void
    {
        $instance = $this->newInstance(null, 5);
        $instance->setMax(1);
        $this->assertEquals(1, $instance->getMax());
    }

    protected function newInstance(?float $defaultMin = null, ?float $defaultMax = null): Number
    {
        $instance = new Number('key');
        Reflection::setHiddenProperty($instance, 'min', $defaultMin);
        Reflection::setHiddenProperty($instance, 'max', $defaultMax);
        return $instance;
    }
}
