<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use Exception;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;

class SetStepTest extends TestCase
{
    #[Test]
    public function basic(): void
    {
        $newVal = rand(1, 50);
        $instance = $this->newInstance();
        $instance->setStep($newVal);
        $storedValue = Reflection::getHiddenProperty($instance, 'step');
        $this->assertEquals($newVal, $storedValue);
    }

    #[Test]
    public function valueLowerThanZero(): void
    {
        $this->expectException(Exception::class);
        $newVal = rand(-50, 0);
        $instance = $this->newInstance();
        $instance->setStep($newVal);
    }

    #[Test]
    public function valueLowerThanStored(): void
    {
        $instance = $this->newInstance();
        Reflection::setHiddenProperty($instance, 'step', 5);
        $instance->setStep(1);
        $storedValue = Reflection::getHiddenProperty($instance, 'step');
        $this->assertEquals(1, $storedValue);
    }

    protected function newInstance(): Number
    {
        $instance = new Number('key', 1);
        return $instance;
    }
}
