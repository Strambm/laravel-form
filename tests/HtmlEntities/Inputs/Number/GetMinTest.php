<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;

class GetMinTest extends TestCase
{
    #[Test]
    public function getMinProperty(): void
    {
        $instance = new Number('test');
        Reflection::setHiddenProperty($instance, 'min', 1);
        $this->assertEquals(1, $instance->getMin());
    }
}
