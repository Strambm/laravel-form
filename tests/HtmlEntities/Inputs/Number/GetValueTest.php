<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetValueTest as InputGetValueTest;

class GetValueTest extends InputGetValueTest
{
    #[Test]
    public function testMethod(): void
    {
        $expectedValue = '13';
        $instance = new Number('test', $expectedValue);
        $this->assertEquals($expectedValue, $instance->getValue());
    }

    #[Test]
    public function empty(): void
    {
        $instance = new Number('test');
        $this->assertEquals('', $instance->getValue());
    }

    #[Test]
    public function withStep(): void
    {
        $instance = new Number('test', 123456);
        $instance->setStep(0.152);
        $this->assertEquals('123456.000', $instance->getValue());
        $this->assertEquals('123456', Reflection::getHiddenProperty($instance, 'value'));
    }
}
