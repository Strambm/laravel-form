<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetInputTypeTest as InputGetInputTypeTest;

class GetInputTypeTest extends InputGetInputTypeTest
{
    #[Test]
    public function testMethod(): void
    {
        $instance = new Number('test', 12);
        $this->assertEqualsIgnoringCase('number', $instance->getInputTypeAttribute());
    }
}
