<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use function rand;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use Cetria\Laravel\Form\Exception\BadNumberBetweenException;

class SetMinTest extends TestCase
{
    #[Test]
    public function setMinProperty(): void
    {
        $value = rand(-999999999, 999999999999);
        $instance = $this->newInstance();
        $instance->setMin($value);
        $this->assertEquals($value, $instance->getMin());
    }

    #[Test]
    public function valueGreaterThanMax(): void
    {
        $this->expectException(BadNumberBetweenException::class);
        $value = rand(0, 999999999999);
        $instance = $this->newInstance(null, -1);
        $instance->setMin($value);
    }

    #[Test]
    public function valueLowerThanStored(): void
    {
        $instance = $this->newInstance(5);
        $instance->setMin(1);
        $this->assertEquals(1, $instance->getMin());
    }

    #[Test]
    public function valueLowerThanStoredWithoutAbsolute(): void
    {
        $instance = $this->newInstance(5);
        $instance->setMin(1, false);
        $this->assertEquals(5, $instance->getMin());
    }

    #[Test]
    public function testMethod__valueLowerThanStored__withAbsolute(): void
    {
        $instance = $this->newInstance(5);
        $instance->setMin(1, true);
        $this->assertEquals(1, $instance->getMin());
    }

    #[Test]
    public function valueGreaterThanStored(): void
    {
        $instance = $this->newInstance(5);
        $instance->setMin(6);
        $this->assertEquals(6, $instance->getMin());
    }

    protected function newInstance(?float $defaultMin = null, ?float $defaultMax = null): Number
    {
        $instance = new Number('key');
        Reflection::setHiddenProperty($instance, 'min', $defaultMin);
        Reflection::setHiddenProperty($instance, 'max', $defaultMax);
        return $instance;
    }
}
