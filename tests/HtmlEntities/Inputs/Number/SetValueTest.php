<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use stdClass;
use Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\SetValueTest as InputSetValueTest;

class SetValueTest extends InputSetValueTest
{
    public static function setValueDataProvider(): array
    {
        return [
            [
                15
            ], [
                null
            ], [
                ''
            ], [
                15.5
            ], [
                '16.2'
            ]
        ];
    } 

    public static function badInputDataProvider(): array
    {
        return [
            [
                [
                    '1'
                ]
            ], [
                new stdClass
            ], [
                'val1'
            ]
        ];
    }

    protected function getTestObjectClass(): string
    {
        return Number::class;
    }
}
