<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Number;

use PHPUnit\Framework\Attributes\Test;
use TypeError;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;
use Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\ConstructorTest as InputConstructorTest;

class ConstructorTest extends InputConstructorTest
{
    public static function setValueParameterDataProvider(): array
    {
        return [
            [
                15,
            ], [
                45.5
            ], [
                '75.5',
            ], [
                ''
            ], [
                null
            ]
        ];
    }

    #[Test]
    public function setParameterAsStringText(): void
    {
        $this->expectException(TypeError::class);
        $this->makeInstance('name', 'value');
    }

    protected function getTestObjectClass(): string
    {
        return Number::class;
    }
}
