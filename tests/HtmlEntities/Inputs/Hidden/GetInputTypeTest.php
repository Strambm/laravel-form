<?php

namespace Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Hidden;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Hidden;
use Cetria\Laravel\Form\Tests\HtmlEntities\Inputs\Input\GetInputTypeTest as InputGetInputTypeTest;

class GetInputTypeTest extends InputGetInputTypeTest
{   
    #[Test]
    public function testMethod(): void
    {
        $instance = new Hidden('test', 'test');
        $this->assertEqualsIgnoringCase('hidden', $instance->getInputTypeAttribute());
    }
}
