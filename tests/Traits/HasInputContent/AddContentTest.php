<?php

namespace Cetria\Laravel\Form\Tests\Traits\HasInputContent;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class AddContentTest extends TestCase
{
    #[Test]
    public function overide(): void
    {
        $key = 'key';
        $block = $this->getTraitHasInputContent();
        $block->addContent((new InputBlock())->setNamePrefix($key));
        $block->addContent((new InputBlock())->setNamePrefix($key));
        $content = $block->getContent();
        $this->assertCount(1, $content);
        $this->assertArrayHasKey($key, $content);
    }

    #[Test]
    public function instanceOfInput(): void
    {
        $inputName = 'inputName';
        $input = new Input($inputName);
        $block = $this->getTraitHasInputContent();
        $block->addContent($input);
        $result = $block->getContent();
        $this->assertArrayHasKey($inputName, $result);
    }
}
