<?php

namespace Cetria\Laravel\Form\Tests\Traits\HasInputContent;

use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class SetNamePrefixTest extends TestCase
{
    #[Test]
    public function basic(): void
    {
        $value = Str::random(10);
        $trait = $this->getTraitHasInputContent();
        $trait->setNamePrefix($value);
        $this->assertEquals($value, $trait->getNamePrefix());
        $this->assertEquals($value, Reflection::getHiddenProperty($trait, 'namePrefix'));
    }

    #[Test]
    public function recursive(): void
    {
        $inputBlock2 = new InputBlock();
        $inputBlock2->setNamePrefix('level2')
            ->addContent(new Input('i1'))
            ->addContent(new Input('i2'));
        $inputBlock1 = new InputBlock();
        $inputBlock1->setNamePrefix('level1')
            ->addContent(new Input('i1'))
            ->addContent($inputBlock2);

        $content1 = $inputBlock1->getContent();
        $this->assertCorrectContentEntity($content1, 'level1i1', Input::class);
        $this->assertCorrectContentEntity($content1, 'level1level2', InputBlock::class);
        /** @var InputBlock $content1Block */
        $content1Block = $content1['level1level2'];
        $this->assertCorrectContentEntity($content1Block->getContent(), 'level1level2i1', Input::class);
        $this->assertCorrectContentEntity($content1Block->getContent(), 'level1level2i1', Input::class);
    }

    protected function assertCorrectContentEntity(array $content, string $expectedName, string $expectedInstanceOf): void
    {
        $entity = $content[$expectedName];
        $this->assertInstanceOf($expectedInstanceOf, $entity);
        if($entity instanceof Input) {
            $this->assertEquals($expectedName, $entity->getName());
        } else {
            $this->assertEquals($expectedName, $entity->getNamePrefix());
        }
    }
}
