<?php

namespace Cetria\Laravel\Form\Tests\Traits\HasInputContent;

use TypeError;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class SetContentTest extends TestCase
{
    #[Test]
    public function overide(): void
    {
        $block = $this->getTraitHasInputContent();
        $block->addContent(new Input('before'));
        $block->setContent([
            new Input('After')
        ]);
        $this->assertCount(1, $block->getContent());
        $this->assertArrayHasKey('After', $block->getContent());
    }

    #[Test]
    public function fakeKeys(): void
    {
        $block = $this->getTraitHasInputContent();
        $block->setContent([
            'test' => new Input('After')
        ]);
        $this->assertCount(1, $block->getContent());
        $this->assertArrayHasKey('After', $block->getContent());
    }

    #[Test]
    public function badInputFormat(): void
    {
        $this->expectException(TypeError::class);
        $block = $this->getTraitHasInputContent();
        $block->setContent([
            'test' => 15
        ]);
    }
}
