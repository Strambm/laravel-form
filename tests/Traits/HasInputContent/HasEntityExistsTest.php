<?php

namespace Cetria\Laravel\Form\Tests\Traits\HasInputContent;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class HasEntityExistsTest extends TestCase
{
    #[Test]
    public function hasEntityExists(): void
    {
        $block = $this->getTraitHasInputContent();
        $block->addContent(new Input('test'));
        $this->assertTrue($block->hasEntityExists('test'));
        $this->assertFalse($block->hasEntityExists('test1'));
    }

    #[Test]
    public function hasEntityExistsRecursive(): void
    {
        $block = $this->getTraitHasInputContent();
        $inputBlock = new InputBlock();
        $input = new Input('test');
        $inputBlock->addContent($input);
        $block->addContent($inputBlock, 'block');
        $this->assertTrue($block->hasEntityExists('test'));
    }
}
