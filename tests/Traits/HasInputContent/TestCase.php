<?php

namespace Cetria\Laravel\Form\Tests\Traits\HasInputContent;

use PHPUnit\Framework\TestCase as ParentTestCase;
use Cetria\Laravel\Form\Traits\HasInputContent;

class TestCase extends ParentTestCase
{
    protected function getTraitHasInputContent(): object
    {
        $class = new class() 
        {
            use HasInputContent;
        };
        return $class;
    }
}
