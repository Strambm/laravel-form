<?php

namespace Cetria\Laravel\Form\Tests\Traits\HasInputContent;

use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;

class ChangeNamePrefixTest extends TestCase
{
    #[Test]
    public function rename(): void
    {
        $value1 = 'v1';
        $value2 = 'v2';
        $value3 = 'v3';
        $trait = $this->getTraitHasInputContent();
        $trait->setNamePrefix($value1 . $value2);
        $trait->changeNamePrefix($value1, $value3);
        $this->assertEquals($value3.$value2, $trait->getNamePrefix());
    }

    #[Test]
    public function setNameAttribute(): void
    {
        $value1 = Str::random(10);
        $trait = $this->getTraitHasInputContent();
        $trait->changeNamePrefix('', $value1);
        $this->assertEquals($value1, $trait->getNamePrefix());
    }

    #[Test]
    public function renameContent(): void
    {
        $name = Str::random(10);
        $prefix = Str::random(10);
        $trait = $this->getTraitHasInputContent();
        $trait->addContent(new Input($name));
        $trait->changeNamePrefix('', $prefix);
        /** @var Input $contentInput */
        $contentInput = $trait->getContent()[$prefix . $name];
        $this->assertEquals($prefix . $name, $contentInput->getName());
    }
}
