<?php

namespace Cetria\Laravel\Form\Tests\Traits\HasInputContent;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Cetria\Laravel\Form\Exception\AttributeDoesNotExistException;

class GetContentEntityTest extends TestCase
{
    #[Test]
    public function unexistsKey(): void
    {
        $this->expectException(AttributeDoesNotExistException::class);
        $block = $this->getTraitHasInputContent();
        $block->getContentEntity('test');
    }

    #[Test]
    public function getContentEntity(): void
    {
        $block = $this->getTraitHasInputContent();
        $input1 = new Input('input1');
        $block->addContent(new Input('noise'));
        $block->addContent($input1);
        $result = $block->getContentEntity('input1');
        $this->assertEquals($input1, $result);
    }
}
