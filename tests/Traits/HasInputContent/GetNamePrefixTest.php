<?php

namespace Cetria\Laravel\Form\Tests\Traits\HasInputContent;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Helpers\Reflection\Reflection;

class GetNamePrefixTest extends TestCase
{
    #[Test]
    public function empty(): void
    {
        $block = $this->getTraitHasInputContent();
        $block->setNamePrefix('testVal');
        $this->assertEquals('testVal', Reflection::getHiddenProperty($block, 'namePrefix'));
        $this->assertEquals('testVal', $block->getNamePrefix());
    }
}
