<?php

namespace Cetria\Laravel\Form\Tests\Traits\UseCastToForm;

use PHPUnit\Framework\Attributes\DataProvider;
use stdClass;
use Exception;
use Cetria\Laravel\Form\Form;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\Traits\UseCastToForm;
use Cetria\Laravel\Form\HtmlEntities\InputBlock;
use Cetria\Laravel\Form\HtmlEntities\DynamicBlock;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Input;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Cetria\Laravel\Form\Exception\UnsupportedMethodException;

class ExtensionFormForRelatedModelsTest extends TestCase
{
    #[Test]
    public function empty(): void
    {
        $testTrait = $this->getTestTrait();
        $result = $this->act($testTrait);
        $this->assertCount(0, $result->getContent());
    }

    #[Test]
    public function relationDoesntExists(): void
    {
        $this->expectException(Exception::class);
        $testTrait = $this->getTestTrait(['testRelation']);
        $this->act($testTrait);
    }

    #[Test]
    public function relatedModelIsntInstanceOfCastToForm(): void
    {
        $this->expectException(UnsupportedMethodException::class);
        $testTrait = $this->getTestTrait(['testRelation' => stdClass::class]);
        $this->act($testTrait);
    }

    #[Test]
    #[DataProvider('relationDataProvider')]
    public function relation(string $relationName, string $expectedBlockClass): void
    {
        $testTrait = $this->getTestTrait([$relationName]);
        $result = $this->act($testTrait);
        $formContent = $result->getContent();
        $this->assertCount(1, $formContent);
        $this->assertCorrectChildrenForm($formContent, $relationName . '.', $expectedBlockClass);
    }

    public static function relationDataProvider(): array
    {
        return [
            [
                'testRelationHasOne',
                InputBlock::class,
            ], [
                'testRelationHasMany',
                DynamicBlock::class,
            ], [
                'testRelationBelongsTo',
                InputBlock::class,
            ], 
        ];
    }

    #[Test]
    public function hasOneWithExpectedClassIsDifferentThanRelated(): void
    {
        $relatedModel = new class extends Model implements CastToFormInterface
        {
            use UseCastToForm;

            protected $fillable = [
                'idk',
                'ddd',
                'ee',
            ];
        };
        $testTrait = $this->getTestTrait(['testRelationHasOne' => get_class($relatedModel)]);
        $result = $this->act($testTrait);
        $formContent = $result->getContent();
        $this->assertCount(1, $formContent);
        $this->assertCount(3, current($formContent)->getContent());
    }

    protected function act($trait): Form
    {
        $methodName = 'extensionFormForRelatedModels';
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait, new Form());
    }

    protected function getTestTrait(array $toFormRelatedModels = [])
    {
        $relatedModel = new class extends Model implements CastToFormInterface
        {
            use UseCastToForm;

            protected $fillable = [
                'idk',
                'ddd',
            ];
        };

        $trait = new class() extends Model implements CastToFormInterface
        {
            use UseCastToForm;

            public static  $related = null;

            public function testRelationHasOne(): HasOne
            {
                return new HasOne(static::$related->newQuery(), $this, 'related_id', 'id');
            }

            public function testRelationHasMany(): HasMany
            {
                return new HasMany(static::$related->newQuery(), $this, 'related_id', 'id');
            }

            public function testRelationBelongsTo(): BelongsTo
            {
                return new BelongsTo(static::$related->newQuery(), $this, 'related_id', 'id', $this->guessBelongsToRelation());
            }
        };
        $trait::$related = $relatedModel;

        Reflection::setHiddenProperty($trait, 'toFormRelatedModels', $toFormRelatedModels);
        return $trait;
    }

    protected function assertCorrectChildrenForm(array $content, string $prefix, string $expectedClassOf): void
    {
        $this->assertArrayHasKey($prefix, $content);
        $entity = $content[$prefix];
        $this->assertEquals($expectedClassOf, get_class($entity));
        $entityContent = $entity->getContent();
        $this->assertCount(2, $entityContent);
        $this->assertCount(1, array_filter($entityContent, function(Input $input) use ($prefix) {
            return $input->getName() == $prefix . 'idk';
        }));
        $this->assertCount(1, array_filter($entityContent, function(Input $input) use ($prefix) {
            return $input->getName() == $prefix . 'ddd';
        }));
    }
}
