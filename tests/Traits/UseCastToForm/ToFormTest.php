<?php

namespace Cetria\Laravel\Form\Tests\Traits\UseCastToForm;

use Cetria\Laravel\Form\Form;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Form\Traits\UseCastToForm;
use Cetria\Laravel\Form\HtmlEntities\Inputs\Number;

class ToFormTest extends TestCase
{
    #[Test]
    public function createdModel(): void
    {
        $name = 'amount';
        $mock = $this->mockTrait([$name], [$name => 'integer']);
        /** @var Form */
        $form = $mock->toForm();
        $inputs = $form->getContent();
        $this->assertCount(1, $inputs);
        $this->assertArrayHasKey($name, $inputs);
        /** @var Number $input */
        $input = $inputs[$name];
        $this->assertInstanceOf(Number::class, $input);
        $this->assertEquals($name, $input->getName());
        $this->assertEquals(null, $input->getValue());
    }

    #[Test]
    public function separatePrimaryKey(): void
    {
        $name = 'amount';
        $primaryKeyName = 'id';
        $mock = $this->mockTrait([$name], [$name => 'integer', $primaryKeyName => 'integer',]);
        /** @var Form */
        $form = $mock->toForm();
        $inputs = $form->getContent();
        $this->assertCount(1, $inputs);
        /** @var Number $input */
        $input = $inputs[$name];
        $this->assertInstanceOf(Number::class, $input);
        $this->assertEquals($name, $input->getName());
    }

    #[Test]
    public function withValidator(): void
    {
        $name = 'amount';
        $mock = $this->mockTrait([$name], [], [$name => 'digits_between:1,2']);
        /** @var Form */
        $form = $mock->toForm();
        $inputs = $form->getContent();
        $this->assertCount(1, $inputs);
        /** @var Number $input */
        $input = $inputs[$name];
        $this->assertInstanceOf(Number::class, $input);
        $this->assertEquals(0, $input->getMin());
        $this->assertEquals(99, $input->getMax());
    }

    private function mockTrait(array $fillables = [], array $casts = [], array $validator = [])
    {
        $trait = new class($fillables, $casts, $validator)
        {
            use UseCastToForm;

            private $fillables = null;
            private $casts = null;
            private $validator = null;

            public function __construct(array $fillables, array $casts, array $validator)
            {
                $this->fillables = $fillables;
                $this->casts = $casts;
                $this->validator = $validator;
            }

            public function getFillable(): array
            {
                return $this->fillables;
            }

            public function getValidator(): array
            {
                return $this->validator;
            }

            public function getCasts(): array
            {
                return $this->casts;
            }

            protected function getAttributeFromArray($key)
            {
                return null;
            }
        };
        return $trait;
    }
}
