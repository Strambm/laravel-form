<?php

namespace Cetria\Laravel\Form\Tests\Traits\UseCastToForm;

use PHPUnit\Framework\Attributes\DataProvider;
use stdClass;
use Exception;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Form\Traits\UseCastToForm;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Cetria\Laravel\Form\Interfaces\CastToFormInterface;
use Cetria\Laravel\Form\Exception\UnsupportedMethodException;

class GetToFormRelatedModelsTest extends TestCase
{
    #[Test]
    public function relationDoesntExists(): void
    {
        $this->expectException(Exception::class);
        $testTrait = $this->getTestTrait(['testRelation']);
        $testTrait->getToFormRelatedModels();
    }

    #[Test]
    public function relatedModelIsntInstanceOfCastToForm(): void
    {
        $this->expectException(UnsupportedMethodException::class);
        $testTrait = $this->getTestTrait(['testRelation' => stdClass::class]);
        $testTrait->getToFormRelatedModels();
    }

    #[Test]
    #[DataProvider('relationDataProvider')]
    public function relation(string $relationName): void
    {
        $testTrait = $this->getTestTrait([$relationName]);
        $result = $testTrait->getToFormRelatedModels();
        $this->assertArrayHasKey($relationName, $result);
        $this->assertEquals(get_class($testTrait::$related), $result[$relationName]);
    }

    public static function relationDataProvider(): array
    {
        return [
            [
                'testRelationHasOne',
            ], [
                'testRelationHasMany',
            ], [
                'testRelationBelongsTo',
            ], 
        ];
    }

    protected function getTestTrait(array $toFormRelatedModels = [])
    {
        $relatedModel = new class extends Model implements CastToFormInterface
        {
            use UseCastToForm;

            protected $fillable = [
                'idk',
                'ddd',
            ];
        };

        $trait = new class() extends Model implements CastToFormInterface
        {
            use UseCastToForm;

            public static  $related = null;

            public function testRelationHasOne(): HasOne
            {
                return new HasOne(static::$related->newQuery(), $this, 'related_id', 'id');
            }

            public function testRelationHasMany(): HasMany
            {
                return new HasMany(static::$related->newQuery(), $this, 'related_id', 'id');
            }

            public function testRelationBelongsTo(): BelongsTo
            {
                return new BelongsTo(static::$related->newQuery(), $this, 'related_id', 'id', $this->guessBelongsToRelation());
            }
        };
        $trait::$related = $relatedModel;

        Reflection::setHiddenProperty($trait, 'toFormRelatedModels', $toFormRelatedModels);
        return $trait;
    }
}
