# cetria/laravel-from #

Nádstavba pro Laravel framework určená pro generování formulářů především pro Eloquent modely, kde se inputy předgenerují podle attributů casts.

### Instalace ###

Pro instalaci balíčku je nutné jej instalovat skrze [composer](https://getcomposer.org/).

```bash
composer require cetria/laravel-form
```

Po stažení knihovny zkopírujte resources příkazem:

```bash
php artisan vendor:publish --tag=cetria-form
```

Rozšiřte svoje modely:

```php
class YourDummyModel extemds \Illuminate\Database\Eloquent\Model
implements \Cetria\Laravel\Form\CastToFormInterface
{
	use \Cetria\Laravel\Form\UseCastToForm
}
```

A poté můžete na svých modelech volat novou metodu:

```php
$model = new YoutDummyModel();
$form = $model->toForm();
```

### Kontakt ###

Pište bugreport na email strambm@gmail.com