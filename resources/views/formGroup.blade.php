<div class="cetria-form__form-group">
    @if($attribute->hasLabel())
        <label 
            class="cetria-form__label" 
            for="{{ $attribute->getId() }}"
        >
            {{ $attribute->getLabel() }}
        </label>
    @endif
    @if(view()->exists('nazev_viewu'))
    @include('cetria-form::inputs.' . $attribute->getInputTypeAttribute(), ['attribute' => $attribute])
    @else
        @include('cetria-form::inputs.input', ['attribute' => $attribute])
    @endif
    
</div>