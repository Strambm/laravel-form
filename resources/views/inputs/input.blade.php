<input 
    type="{{ $attribute->getInputTypeAttribute() }}" 
    name="{{ $attribute->getName() }}" 
    value="{{ $attribute->getValue() }}"
    id="{{ $attribute->getId() }}"
    class="cetria-form__attribute cetria-form__attribute--type-{{ $attribute->getInputTypeAttribute() }}"
    {{ $attribute->getRequired() ? 'required' : '' }}
    @if (View::hasSection('specific-attributes'))
        @yield('specific-attributes')
    @endif
/>