@extends('cetria-form::attribute.attribute')

@section('specific-attributes')
    step="{{ $attribute->getStep() }}"
    @if($attribute->getMin())
        min="{{ $attribute->getMin() }}"
    @endif
    @if($attribute->getMax())
        max="{{ $attribute->getMax() }}"
    @endif
@endsection