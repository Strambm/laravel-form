<select name="{{ $attribute->getName() }}">
    @foreach($attribute->getOptions() as $option)
        <option value="{{ $option->getValue() }}">{{ $option->getDescription() }}</option>
    @endforeach
</select>