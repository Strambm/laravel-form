<form 
    method="{{ $form->getParams()->getMethod() }}"
    action="{{ $form->getParams()->getAction() }}"
    class="cetria-form"
>
    @foreach($form->getAttributes() as $attribute)
        {{ $attribute->render() }}
    @endforeach
</form>
